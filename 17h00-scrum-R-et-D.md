# SCRUM et R&D : l'organisation dans la désorganisation / Albert-Antoine NOEL

Retour d'expérience sur la mise en place d'une gestion de projet en SCRUM organisée dans un contexte et chez un client très désorganisé Discussions autour de problèmes tel que : la mauvaise gestion du besoin, le changement d'avis trop soutenu et la mauvaise communication sur le produit auprès du client