# agile-tour

My notes on agile tour

## Programme

http://at2019.agiletour.org/strasbourg_programme.html

| Horaires  | AMPHI | SALLE 105 |
| --------- | ----- | --------- |
| 13:30	    | Accueil | |
| 14:00	    | Lancement | |
| 14:30	    | Sprint Ø ? L’environnement d’un début de projet  François SIMOND & Rémi BENOIT | GitHub et Microsoft Azure DevOps - Le mariage parfait Adrien CLERBOIS |
| 15:30	    | L’Agilité pour remettre le développeur au centre du jeu  Valentin METZ & Thomas MESSMER	| Everything-OPS / Dev(No)Ops – le DevOps est-il dejà hasbeen ?  Philippe DIDIERGEORGES |
| 16:30	    | Pause | |
| 17:00	    | SCRUM et R&D : l'organisation dans la désorganisation  Albert-Antoine NOEL | Remettons les Tests au coeur des projets  Christophe GIGAX & Rémi BENOIT |
| 18:00	    | Holacratie : les valeurs de l'agilité au cœur de la gouvernance  Alexandre RICHARD | REX Vestiaire Collective - Soyez Agile qu'il disait  Olivier ROUHAUD |
| 19:00	    | Fin | |

## notes

