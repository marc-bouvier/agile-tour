# Sprint Ø ? L’environnement d’un début de projet / François SIMOND & Rémi BENOIT

Sprint 0, avant-projet, ces termes désignent les activités avant le premier sprint d'un projet. Mais que regroupe exactement ces activités ? Quels sont les indicateurs pour clore cette étape et démarrer sereinement le premier sprint ? Un guide bien connu parle de prélude à Scrum, nous le présenterons au travers des voyants nécessaires à la validation d’une équipe prête à Scrum, la création du backlog initial et la mise en place des briques techniques nécessaires à la réalisation des US. La présentation sera axée autour de nos retours d’expérience de nos projets chez Versusmind.

